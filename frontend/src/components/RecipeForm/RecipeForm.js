import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";

class RecipeForm extends Component {
    state = {
        title: '',
        image:'',
        description: '',
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text" required
                    onChange={this.inputChangeHandler}
                    value={this.state.title}
                />

                <FormElement
                    propertyName="description"
                    title="Description"
                    type="textarea" required
                    value={this.props.description}
                    onChange={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="image"
                    title="Recipe Image"
                    type="file" required
                    onChange={this.fileChangeHandler}
                />

                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Create recipe</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default RecipeForm;