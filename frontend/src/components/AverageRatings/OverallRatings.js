import React from 'react';
import StarRatings from "react-star-ratings";

const OverallRating = (props) => {
    return (
        <div>
            <StarRatings
                rating={props.numOverall}
                ratingToShow={props.numOverall}
                starDimension={'25px'}
                starRatedColor="orange"
                numberOfStars={5}
            />
            <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numOverall}
                </span>
        </div>
    );
};

export default OverallRating;