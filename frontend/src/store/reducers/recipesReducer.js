import {FETCH_RECIPES_SUCCESS, FETCH_SINGLE_RECIPE_SUCCESS} from "../actions/recipesActions";

const initialState = {
    recipes: [],
    singleRecipe: null
};

const recipesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_RECIPES_SUCCESS:
            return {...state, recipes: action.recipes};
        case FETCH_SINGLE_RECIPE_SUCCESS:
            return {...state, singleRecipe: action.recipe};
        default:
            return state;
    }
};

export default recipesReducer;
