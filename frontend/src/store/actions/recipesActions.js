import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_RECIPES_SUCCESS = 'FETCH_RECIPES_SUCCESS';
export const FETCH_SINGLE_RECIPE_SUCCESS = 'FETCH_SINGLE_RECIPE_SUCCESS';
export const CREATE_RECIPE_SUCCESS = 'CREATE_RECIPE_SUCCESS';

export const fetchRecipesSuccess = recipes => ({type: FETCH_RECIPES_SUCCESS, recipes});
export const fetchSingleRecipeSuccess = recipe => ({type: FETCH_SINGLE_RECIPE_SUCCESS, recipe});
export const createRecipeSuccess = () => ({type: CREATE_RECIPE_SUCCESS});

export const fetchRecipes = () => {
    return dispatch => {
        return axios.get('/recipes').then(
            response => dispatch(fetchRecipesSuccess(response.data))
        );
    };
};

export const fetchSingleRecipe =(id)=>{
    return dispatch =>{
        return axios.get(`/recipes/${id}`).then(
            response => dispatch(fetchSingleRecipeSuccess(response.data))
        )
    };
};

export const createRecipe = (recipeData) => {
    return dispatch => {
        return axios.post('/recipes', recipeData).then(
            response => {
                dispatch(createRecipeSuccess(response.data));

            }
        );
    };
};

export const createPhoto = (recipeData, id, redirect = true) => {
    return dispatch => {
        return axios.post(`/recipes/${id}`, recipeData).then(
            response => {
                dispatch(createRecipeSuccess(response.data));
                if (redirect) {
                    dispatch(push('/recipes/' + response.data._id));
                }
            },
            error => {

            }
        )
    }
};