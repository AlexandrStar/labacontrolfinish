import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import RecipeForm from "../../components/RecipeForm/RecipeForm";
import {createRecipe} from "../../store/actions/recipesActions";


class NewRecipe extends Component {

    createRecipe = recipeData => {
        this.props.onRecipeCreated(recipeData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                {this.props.user ?
                    <div>
                        <h2>New recipe</h2>
                        <RecipeForm
                            onSubmit={this.createRecipe}
                        />
                    </div> : <h3>А я вам нос сломаю! Если вы не зарегистрируетесь!</h3>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    onRecipeCreated: recipeData => dispatch(createRecipe(recipeData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewRecipe);
