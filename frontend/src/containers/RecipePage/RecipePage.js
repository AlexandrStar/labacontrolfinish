import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardImg, CardText, Col, Row} from "reactstrap";
import {fetchRecipes} from "../../store/actions/recipesActions";
import {connect} from "react-redux";
import config from "../../config";
import {Link} from "react-router-dom";

class RecipePage extends Component {

    componentDidMount() {
        this.props.onFetchRecipes();
    }

    render() {
        console.log(this.props.recipes);
        return (
            <Fragment>
                <Row>
                {this.props && this.props.recipes.map((recipe, index) => (
                    <Col key={index} sm={3}>
                        <Card className="text-center" style={{marginBottom: '20px'}} >
                            <CardBody>
                                <Link to={`/recipe/${recipe._id}`}>
                                    <CardImg top width="100%" src={config.apiURL + '/uploads/' + recipe.image} alt="recipe" />
                                    <CardText style={{paddingTop: '10px'}}>{recipe.title}</CardText>
                                </Link>
                                <CardText>
                                    <small className="text-muted">
                                        (
                                            {recipe.numOverall ? recipe.numOverall : 0}, {recipe && recipe.countComment ? recipe.countComment.count: 0} Reviews
                                        )
                                    </small>
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                ))}
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    recipes: state.recipes.recipes,
});

const mapDispatchToProps = dispatch => ({
    onFetchRecipes: () => dispatch(fetchRecipes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RecipePage);