import React, {Component} from 'react';
import {connect} from "react-redux";
import {createPhoto, fetchSingleRecipe} from "../../store/actions/recipesActions";
import config from "../../config";
import AddComment from "../AddComment/AddComment";
import Comments from "../Comments/Comments";
import Slider from "react-slick";
import "./SlickCarousel.css";
import "./OneRecipePage.css";
import {Button, Card, CardImg, CardText, CardTitle, Col, Form, FormGroup, Input, Label, Row, Spinner} from "reactstrap";

class OneRecipePage extends Component {
    state={
        gallery: [],
    };

    componentDidMount() {
        this.props.onFetchSingleRecipe(this.props.match.params.id)
    }

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                        gallery: [...this.state.gallery, file],
                    });
                };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createPhoto(formData, this.props.match.params.id);
    };

    render() {
        if (this.props.recipe === null) return <Spinner color="success" />;

        const permissionAddComments = (this.props.recipe.user) !== (this.props.user && this.props.user._id) && (this.props.user && this.props.user.role);

        const permissionAddPictures = (this.props.recipe.user) === (this.props.user && this.props.user._id);

        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };

        return (
            <div>
                <Card className="border-0">
                    <Row>
                        <Col sm={8}>
                            <CardTitle><h1>{this.props.recipe.title}</h1></CardTitle>
                            <CardText>{this.props.recipe.description}</CardText>
                        </Col>
                        <Col sm={4}>
                            <CardImg
                                src={config.apiURL + '/uploads/' + this.props.recipe.image}
                                alt={this.props.recipe.title}
                            />
                        </Col>
                    </Row>
                </Card>
                <hr/>
                <div className="gallery-slider">
                    <Slider {...settings}>
                        {
                            this.props.recipe.gallery.map(slider => (
                                <img key={slider} className='Slide-img' src={config.apiURL + '/uploads/' + slider} alt={this.props.recipe.name}/>
                            ))
                        }
                    </Slider>
                </div>
                <div>
                    <Comments recipeId={this.props.match.params.id}/>
                    {permissionAddComments ?
                        <AddComment recipeId={this.props.match.params.id}/>
                    : null}
                </div>
                <hr/>
                {permissionAddPictures ?
                    <Form onSubmit={this.submitFormHandler}>
                        <FormGroup row>
                            <Label for="gallery" sm={2}>Галерея</Label>
                            <Col sm={10}>
                                <Input
                                    type="file"
                                    name="gallery" id="gallery"
                                    onChange={this.galleryChangeHandler}
                                    multiple
                                />
                            </Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col sm={{offset:2, size: 10}}>
                                <Button type="submit" color="primary">Add Photo</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                : null}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    recipe: state.singleRecipe.singleRecipe,
    user: state.users.user,
    comment: state.comments.comments,
});

const mapDispatchToProps = dispatch => ({
    onFetchSingleRecipe: (id) => dispatch(fetchSingleRecipe(id)),
    createPhoto: (recipeData, id) => dispatch(createPhoto(recipeData, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneRecipePage);