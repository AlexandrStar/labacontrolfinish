import React from 'react';
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import RecipePage from "./containers/RecipePage/RecipePage";
import OneRecipePage from "./containers/OneRecipePage/OneRecipePage";
import NewRecipe from "./containers/NewRecipe/NewRecipe";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={RecipePage}/>
            <Route path="/recipe/:id" exact component={OneRecipePage}/>
            <Route path="/new_recipe" exact component={NewRecipe}/>
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
        </Switch>
    );
};

export default Routes;