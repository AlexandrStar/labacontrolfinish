const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const addAuth = require('../middleware/addAuth');
const permit = require('../middleware/permit');
const Recipe = require('../models/Recipe');
const Comment  = require('../models/Comment');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', addAuth, async (req, res) => {
    try {
        let criteria = {removed: false};

        if (req.query.user) {
            criteria.user = req.query.user;
        }

        const recipes = await Recipe.find(criteria).populate('user', '_id, displayName').sort('title');

        const commentsCount = await Comment.aggregate([{$group: {_id: '$recipeId', count: {$sum: 1}}}]);

        const recipesWithCount = recipes.map(recipe => {
            const recipeCount = commentsCount.find(pc => pc._id.equals(recipe._id));
            return {
                ...recipe.toObject(),
                countComment: recipeCount
            }
        });

        return res.send(recipesWithCount)
    }catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/:id', addAuth, async (req, res) => {
    try {
        const recipe = await Recipe.findById(req.params.id);
        res.send(recipe);
    }catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
    const recipeData = req.body;

    if (req.file) {
        recipeData.image = req.file.filename;
    }

    recipeData.user = req.user._id;

    const recipe = new Recipe(recipeData);

    recipe.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id', auth, upload.fields([{name: 'gallery', maxCount: 10}]), async (req, res) => {
    try {
        const recipeData = await Recipe.findById(req.params.id);

        if (req.files) {
            recipeData.gallery = req.files.gallery ? req.files.gallery.map(file =>  file.filename) : undefined;
        }

        recipeData.user = req.user._id;

        const recipe = new Recipe(recipeData);
        await recipe.save();

        return res.send(recipe);
    } catch (e) {
        console.log(e);
        return res.sendStatus(500);
    }
});

router.post('/:id/toggle_removed', auth, async (req, res) => {
    const recipe = await Recipe.findById(req.params.id);

    if (!recipe) {
        return res.sendStatus(404);
    }

    recipe.removed = !recipe.removed;

    await recipe.save();

    res.send(recipe);
});

router.patch('/', auth, async (req, res) => {
    try {
        const recipe = await Recipe.findById(req.body.recipeId);

        recipe.numOverall = req.body.numOverall.toFixed(1);

        await recipe.save();
        return res.send('ok')
    } catch (e) {
        return res.status(500)
    }
});

module.exports = router;